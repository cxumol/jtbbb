# Thanks to http://stackoverflow.com/questions/9884353/xls-to-csv-convertor
import os, csv, xlrd

# excel_file = 'JQT101-µ¥Î»»ù±¾Çé¿ö.xls'
#we're specifying a directory here because we'll have several files

#csv_filepath = './0/'

def csv_from_excel(excel_file):
    workbook = xlrd.open_workbook(excel_file)
    all_worksheets = workbook.sheet_names()
  
    for worksheet_name in all_worksheets:
        worksheet = workbook.sheet_by_name(worksheet_name)
        your_csv_file = open(''.join([csv_filepath,worksheet_name,'.csv']), 'w', encoding='utf8')
        wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)
        
        for rownum in range(worksheet.nrows):
            wr.writerow(worksheet.row_values(rownum))
        your_csv_file.close()
  
csv_from_excel(excel_file)