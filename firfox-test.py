from time import sleep
import logging
import os
import csv,xlrd

from multiprocessing import Process

USERNAME = '430000_YG'
PASSW = 'Tj82582265'

logging.basicConfig(format="%(asctime)s[%(levelname)s]:%(message)s", datefmt='%Y%m%d,%H:%M:%S', level=logging.INFO)

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

def clk(elem):
    return ActionChains(browser).move_to_element(elem).click(elem).perform()

def waitfind(fn,pr,sec=1):
    '''waitfind(function, perameter, wait seconds)'''
    while 1:
        try:
            return fn(pr)
        except:
            logging.info("waiting on finding "+str(pr)+" for "+str(sec)+" s")
            sleep(sec)

# fp = webdriver.FirefoxProfile(r"%APPDATA%\Mozilla\Firefox\Profiles\")
fp = webdriver.FirefoxProfile()
fp.accept_untrusted_certs = True
fp.set_preference("browser.download.folderList",2)
fp.set_preference("browser.download.manager.showWhenStarting",False)
fp.set_preference("browser.download.dir", os.getcwd())
fp.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/octet-stream")

def new_dir(mypath):
    if not os.path.isdir(mypath):
        os.makedirs(mypath)
    return mypath

def moveto(dst, src=os.getcwd()):
    listOfFiles = os.listdir(src)
    for f in listOfFiles:
        if f.endswith(".xls") or f.endswith(".xlsx"):
            srcPath = src + "/" + f
            dstpath = dst + "/" + f
            try:
                os.rename (srcPath, dstpath)
            except FileExistsError as e:
                logging.fatal(e)
                logging.fatal("Please remove all previous existing files!")
                

def csv_from_excel(idx ,csv_filepath, excel_file):
    workbook = xlrd.open_workbook(excel_file)
    all_worksheets = workbook.sheet_names()
  
    for worksheet_name in all_worksheets:
        worksheet = workbook.sheet_by_name(worksheet_name)
        your_csv_file = open(''.join([csv_filepath,worksheet_name,"_",str(idx),'.csv']), 'w', encoding='utf8')
        wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)
        
        for rownum in range(worksheet.nrows):
            wr.writerow(worksheet.row_values(rownum))
        your_csv_file.close()
    return None

def downloadedHandler(situation_text):
    logging.info('handle downloaded files for %s ...' % situation_text)

    qk_path = "./"+situation_text+"/"
    moveto(new_dir(qk_path))

    listOfFiles = os.listdir(qk_path)
    for i, f in enumerate(listOfFiles):
        
        if f.endswith(".xls") or f.endswith(".xlsx"):
            #csv_from_excel(new_dir(qk_path+"csv/"), qk_path+f)
            csv_from_excel(i, qk_path, qk_path+f)

    cmd=''.join(["copy ", "*.csv all_", situation_text, ".csv"])
    #test
    logging.info("trasfering by "+cmd)
    os.system(''.join(["cd ", qk_path, " && ", cmd, " && move all* .. &&  cd .."]))

browser = webdriver.Firefox(firefox_profile=fp)

# enter
browser.get('https://lwzb.catsic.com/')
#sleep(7)

iframe_Shell = waitfind(browser.find_element_by_class_name,'Shell',3)

# iframe_Shell.send_keys(Keys.CONTROL + Keys.SUBTRACT)
# iframe_Shell.send_keys(Keys.CONTROL + Keys.SUBTRACT)
# browser.execute_script("document.body.style.MozTransform = 'scale(0.7)';")

browser.switch_to.frame(iframe_Shell)

username_elem = waitfind(browser.find_element_by_css_selector,'.text-border-f-l+td .text-input',1)
username_elem.send_keys(USERNAME + Keys.TAB + PASSW + Keys.RETURN)

sleep(25) #force sleep
logging.info('logged on by %s', USERNAME)
logging.info("switch menu")

sdfz_menu = waitfind(browser.find_element_by_id,'_e_91',1)
clk(sdfz_menu)

browser.switch_to.parent_frame()
# zdysj_menu = browser.find_element_by_partial_link_text("自定义数据查询")
sleep(1)
zdysj_menu = browser.find_element_by_id('_e_127')
ActionChains(browser).move_to_element(zdysj_menu).click(zdysj_menu).perform()

sleep(4)
browser.switch_to.frame(iframe_Shell)
situation_items = waitfind(browser.find_elements_by_css_selector,'.Label[title$="况"]',1)
logging.info("moving into condition list")
# zoom out

ActionChains(browser).key_down(Keys.CONTROL).send_keys(Keys.SUBTRACT+Keys.SUBTRACT).key_up(Keys.CONTROL).perform()

for situation_idx, situation_item in enumerate(situation_items,start=1):
    #test
    if situation_idx == 3:break

    logging.info("  into condition: "+str(situation_idx))

    clk(situation_item)
    sleep(5)

    logging.info("  into company list")
    browser.switch_to.parent_frame()
    browser.switch_to.frame(browser.find_elements_by_class_name("Composite")[0])

    allpage = browser.find_element_by_xpath("/html/body/div/div/div[3]/div/div[1]/div[1]/div/div/div[3]/span").text[1:]
    allpage = int(allpage)

    for page_i in range(allpage):

        #test
        if page_i==2: break

        look_items = browser.find_elements_by_css_selector('.Label[title="查看"]')

        for look_idx, look_item in enumerate(look_items,start=1):
            
            #test
            if look_idx == 3:break

            logging.info("  into company: "+str(look_idx)+"/"+str(len(look_items))+" on page "+str(page_i+1))
            clk(look_item)

            sleep(6)
            browser.switch_to.parent_frame()
            browser.switch_to.frame(browser.find_elements_by_class_name("Composite")[0])
            export_btn = browser.find_element_by_css_selector("td[title='Export']") or browser.find_element_by_css_selector("td[title='导出']")
            clk(export_btn)
            #sleep(5) # downloading
            exit2_btn = browser.find_element_by_css_selector("td[title='Export']+td")
            sleep(3)
            clk(exit2_btn)
            sleep(2)
            browser.switch_to.default_content()
            browser.switch_to.frame(browser.find_elements_by_class_name("Composite")[0])

        # browser.switch_to.default_content()
        # browser.switch_to.frame(browser.find_elements_by_class_name("Composite")[0])

        # next_btn = browser.find_elements_by_class_name('btn-link-text')[1]
        # # ActionChains(browser).move_to_element(next_btn).perform()
        # # browser.switch_to.active_element
        # sleep(5)
        # clk(next_btn)

        input_page_i=browser.find_element_by_xpath("/html/body/div/div/div[3]/div/div[1]/div[1]/div/div/div[4]/table/tbody/tr[2]/td[2]/div/input")
        now_page_i = input_page_i.text
        
        try:
            input_page_i.clear()
        except:
            logging.debug("clear() not working here, try conventional typing way")
            input_page_i.send_keys(Keys.CONTROL + 'a')
            input_page_i.send_keys(Keys.DELETE)

        next_page = page_i+2
        logging.info("changing from page "+now_page_i+" to "+str(next_page))
        input_page_i.send_keys(str(next_page))
        input_page_i.click()
        input_page_i.send_keys(Keys.RETURN)

        sleep(7)

    browser.switch_to.default_content()
    close_btn = browser.find_elements_by_class_name('j-tool-close')[0] # close to situations
    clk(close_btn)
    sleep(3)
    browser.switch_to.frame(iframe_Shell)

    try:
        situation_text = situation_item.get_attribute("title")
        if not situation_text: situation_text = "qk_"+str(situation_idx)
    except Exception as e:
        logging.warn("situation text not found, use num instead\n")
        print(e)
        situation_text = "qk_"+str(situation_idx)
    
    logging.debug(situation_text)
    # try:
    #     handle_files.join()
    # except Exception as identifier:
    #     logging.warn(identifier)
    # handle_files = Process( target=downloadedHandler, args=(situation_text,) )
    # handle_files.start()
    downloadedHandler(situation_text)

        

#browser.switch_to.parent_frame()